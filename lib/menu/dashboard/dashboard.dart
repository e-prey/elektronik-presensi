import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:eprey/menu/dashboard/presensi.dart';
import 'package:eprey/menu/history/history.dart';
import 'package:eprey/menu/profile/profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../login_screen.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  bool _visibleFisika = false;
  bool _visibleKimia = false;
  bool _visibleMatematika = false;
  bool _visibleBiologi = false;
  var _emailController = TextEditingController();

  String _name = "";
  String _nim = "";
  String _email = "";

  _getUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _nim = prefs.getString('nim')!;
    _visibleMatematika = prefs.getBool('math$_nim')!;
    _visibleKimia = prefs.getBool('chemical$_nim')!;
    _visibleFisika = prefs.getBool('physics$_nim')!;
    _visibleBiologi = prefs.getBool('biology$_nim')!;

    print(_visibleMatematika);
    print(_visibleBiologi);

    await FirebaseFirestore.instance
        .collection('users')
        .where('nim', isEqualTo: _nim)
        .get()
        .then((value) {
      print(value.size);
      setState(() {
        _name = value.docs.first['name'];
        _email = value.docs.first['email'];
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getUserData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent,
        title: Text("Beranda"),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {
              _showAlertDialog();
            },
            icon: Icon(Icons.menu),
          )
        ],
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                (!_visibleMatematika &&
                        !_visibleKimia &&
                        !_visibleFisika &&
                        !_visibleBiologi)
                    ? Column(
                        children: [
                          SizedBox(
                            height: 32,
                          ),
                          ClipRRect(
                            child: Image.asset(
                              'assets/logo.png',
                              width: 100,
                              height: 100,
                              fit: BoxFit.cover,
                            ),
                            borderRadius: BorderRadius.circular(100),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Text(
                            'Tidak melihat kelas yang sudah ada ?',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Text(
                            'Cobalah akun lainnya',
                            style: TextStyle(
                              color: Colors.blue,
                            ),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Text(
                            'Buat atau gabung dengan kelas pertama anda',
                          ),
                          SizedBox(
                            height: 16,
                          ),
                        ],
                      )
                    : Container(),
                (_visibleMatematika)
                    ? _classBuilder(
                        "https://images.unsplash.com/photo-1584277261846-c6a1672ed979?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=831&q=60",
                        "Matematika",
                        "Cecep Arif Rahman",
                        "Presensi Matematika")
                    : Container(),
                (_visibleFisika)
                    ? _classBuilder(
                        "https://images.unsplash.com/photo-1611784601826-d17011218c7b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=60",
                        "Fisika",
                        "Indah Purnama Sari",
                        "Presensi Fisika")
                    : Container(),
                (_visibleKimia)
                    ? _classBuilder(
                        "https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=60",
                        "Kimia",
                        "Ujang Suparman",
                        "Presensi Kimia")
                    : Container(),
                (_visibleBiologi)
                    ? _classBuilder(
                        "https://images.unsplash.com/photo-1595256899816-bb1d65a24d2a?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=755&q=60",
                        "Biologi",
                        "Andre Sulistyo",
                        "Presensi Biologi")
                    : Container(),
                SizedBox(
                  height: 16,
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.bottomRight,
            margin: EdgeInsets.only(right: 32, bottom: 32),
            child: FloatingActionButton(
              onPressed: () {
                _showAlertDialogAddClass();
              },
              backgroundColor: Colors.lightBlueAccent,
              child: Icon(
                Icons.add,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _showAlertDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            content: Container(
          height: 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 150,
                height: 50,
                margin: EdgeInsets.only(top: 16),
                child: RaisedButton(
                    color: Colors.lightBlueAccent,
                    child: Text(
                      "Kelas",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }),
              ),
              Container(
                width: 150,
                height: 50,
                margin: EdgeInsets.only(top: 16),
                child: RaisedButton(
                    child: Text(
                      "Presensi",
                      style: TextStyle(
                        color: Colors.lightBlueAccent,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                        side: BorderSide(color: Colors.lightBlueAccent)),
                    onPressed: () {
                      Route route =
                          MaterialPageRoute(builder: (context) => History());
                      Navigator.pushReplacement(context, route);
                    }),
              ),
              Container(
                width: 150,
                height: 50,
                margin: EdgeInsets.only(top: 16),
                child: RaisedButton(
                    child: Text(
                      "Profil",
                      style: TextStyle(
                        color: Colors.lightBlueAccent,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                        side: BorderSide(color: Colors.lightBlueAccent)),
                    onPressed: () {
                      Route route =
                          MaterialPageRoute(builder: (context) => Profile());
                      Navigator.pushReplacement(context, route);
                    }),
              ),
              Container(
                width: 150,
                height: 50,
                margin: EdgeInsets.only(top: 16),
                child: RaisedButton(
                    child: Text(
                      "Logout",
                      style: TextStyle(
                        color: Colors.lightBlueAccent,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                        side: BorderSide(color: Colors.lightBlueAccent)),
                    onPressed: () {
                      Navigator.of(context).pop();
                      _logout();
                    }),
              )
            ],
          ),
        ));
      },
    );
  }

  _logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('nim', '');
    prefs.setBool('isLogin', false);

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginScreen()),
        (Route<dynamic> route) => false);
  }

  Widget _classBuilder(
      String image, String subject, String teacher, String desc) {
    return GestureDetector(
      onTap: () {
        Route route = MaterialPageRoute(
            builder: (context) => Presensi(
                  desc: desc,
                ));
        Navigator.push(context, route);
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 150,
        margin: EdgeInsets.only(top: 10, left: 10, right: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            image: DecorationImage(
              image: NetworkImage(image),
              fit: BoxFit.cover,
            )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: Text(
                subject,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                    color: Colors.white),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, top: 50),
              child: Text(
                teacher,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }

  _showAlertDialogAddClass() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: Container(
              height: 400,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        child: Icon(Icons.clear),
                        onTap: () => Navigator.of(context).pop(),
                      ),
                      Text('Gabung ke Kelas'),
                      Text(
                        'Gabung',
                        style: TextStyle(color: Colors.lightBlueAccent),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    thickness: 2,
                    height: 2,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text('Saat ini Anda login sebagai'),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      ClipRRect(
                        child: Image.asset(
                          'assets/logo.png',
                          width: 60,
                          height: 60,
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(100),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.45,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '$_nim $_name',
                              overflow: TextOverflow.ellipsis,
                            ),
                            Text(
                              '$_email',
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                        _logout();
                      },
                      child: Text(
                        'Ganti akun',
                        style: TextStyle(
                          color: Colors.lightBlueAccent,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    thickness: 3,
                    height: 3,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                      'Mintalah kode kelas kepada pengajar, lalu masukkan kode di sini'),
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: TextFormField(
                      controller: _emailController,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        labelText: 'Kode kelas',
                        labelStyle: TextStyle(color: Colors.black),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.lightBlueAccent, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.lightBlueAccent, width: 1.0),
                        ),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Kode kelas tidak boleh kosong';
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    height: 50,
                    margin: EdgeInsets.only(top: 16),
                    child: Center(
                      child: RaisedButton(
                          child: Text(
                            "Cari Kelas",
                            style: TextStyle(
                              color: Colors.lightBlueAccent,
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                              side: BorderSide(color: Colors.lightBlueAccent)),
                          onPressed: () async {
                            if (_emailController.text.toString().isEmpty) {
                              _toast('Kode kelas tidak boleh kosong');
                            } else if (_emailController.text.toString() ==
                                'a001') {
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                              setState(() {
                                if (prefs.getBool('math$_nim') == true) {
                                  _toast('Anda sudah bergabung sebelumnya');
                                } else {
                                  _visibleMatematika = true;
                                  prefs.setBool('math$_nim', true);
                                  _toast(
                                      'Berhasil bergabung di kelas Matematika');
                                }
                              });
                            } else if (_emailController.text.toString() ==
                                'a002') {
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                              setState(() {
                                if (prefs.getBool('physics$_nim') == true) {
                                  _toast('Anda sudah bergabung sebelumnya');
                                } else {
                                  _visibleFisika = true;
                                  prefs.setBool('physics$_nim', true);
                                  _toast('Berhasil bergabung di kelas Fisika');
                                }
                              });
                            } else if (_emailController.text.toString() ==
                                'a003') {
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                              setState(() {
                                if (prefs.getBool('chemical$_nim') == true) {
                                  _toast('Anda sudah bergabung sebelumnya');
                                } else {
                                  _visibleKimia = true;
                                  prefs.setBool('chemical$_nim', true);
                                  _toast('Berhasil bergabung di kelas Kimia');
                                }
                              });
                            } else if (_emailController.text.toString() ==
                                'a004') {
                              print('sasas');
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                              setState(() {
                                if (prefs.getBool('biology$_nim') == true) {
                                  _toast('Anda sudah bergabung sebelumnya');
                                } else {
                                  _visibleBiologi = true;
                                  prefs.setBool('biology$_nim', true);
                                  _toast('Berhasil bergabung di kelas Biologi');
                                }
                              });
                            } else {
                              _toast('Kode tidak cocok pada kelas apapun');
                            }
                          }),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _toast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.lightBlueAccent,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
