import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Presensi extends StatefulWidget {
  final String desc;

  Presensi({
    required this.desc,
  });

  @override
  _PresensiState createState() => _PresensiState();
}

class _PresensiState extends State<Presensi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.grey,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
        title: Text(
          widget.desc,
          style: TextStyle(color: Colors.white),
        ),
        actions: [Padding(
          padding: const EdgeInsets.only(right: 16),
          child: Icon(Icons.image_outlined),
        )],
      ),
      body: Container(
        color: Colors.grey,
        child: Center(
          child: Column(
            children: [
              GestureDetector(
                onTap: () {
                  _showAlertDialog();
                },
                child: Container(
                  color: Color(0xffDBD7D7),
                  width: MediaQuery.of(context).size.width,
                  height: 300,
                  margin: EdgeInsets.only(
                      left: 32,
                      right: 32,
                      top: MediaQuery.of(context).size.height * 0.15),
                  child: Stack(
                    children: [
                      Image.network(
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/QR_code_for_mobile_English_Wikipedia.svg/1200px-QR_code_for_mobile_English_Wikipedia.svg.png",
                        fit: BoxFit.cover,
                        width: 300,
                        height: 300,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 150),
                        child: Divider(
                          height: 3,
                          thickness: 3,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                'Arahkan kamera pada kode QR\nuntuk memulai pemindaian',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 16),
              )
            ],
          ),
        ),
      ),
    );
  }

  _showAlertDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            content: Container(
          height: 300,
          child: Stack(
            children: [

              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                margin:
                    EdgeInsets.only(top: 60, left: 10, right: 10, bottom: 10),
                decoration: BoxDecoration(
                    color: Color(0xff9CE6FD),
                    borderRadius: BorderRadius.circular(15)),
                child: Center(
                  child: Text(
                    'PRESENSI BERHASIL',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(top: 20, left: 65),
                child: Icon(
                  Icons.check_circle,
                  size: 100,
                  color: Colors.blue,
                ),
              ),
            ],
          ),
        ));
      },
    );
  }
}
