import 'package:flutter/material.dart';

class History extends StatefulWidget {
  const History({Key? key}) : super(key: key);

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffDBD7D7),
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent,
        title: Text("Riwayat Presensi"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _baseBuilder('Kelas Fisika\nPertemuan 1', 'Hadir', '8 mei 2021 - 14.15-16.05'),
            _baseBuilder('Kelas Kimia\nPertemuan 1', 'Hadir', '8 mei 2021 - 16.05-18.00'),
            _baseBuilder('Kelas Matematika\nPertemuan 1', 'Hadir', '10 mei 2021 - 08.15-10.05'),
            _baseBuilder('Kelas Biologi\nPertemuan 1', 'Tidak Hadir', '11 mei 2021 - 11.15-13.05'),
            _baseBuilder('Kelas Fisika\nPertemuan 2', 'Hadir', '12 mei 2021 - 8.15-10.05'),
            _baseBuilder('Kelas Kimia\nPertemuan 2', 'Tidak Hadir', '12 mei 2021 - 13.05-15.05'),
            _baseBuilder('Kelas Matematika\nPertemuan 2', 'Hadir', '16 mei 2021 - 08.15-10.05'),
            _baseBuilder('Kelas Biologi\nPertemuan 2', 'Tidak Hadir', '16 mei 2021 - 11.15-13.05'),
          ],
        ),
      ),
    );
  }

  Widget _baseBuilder(String className, String presensi, String waktu, ) {
    return  Container(
      width: MediaQuery.of(context).size.width,
      height: 150,
      margin: EdgeInsets.only(
        top: 10,
        left: 10,
        right: 10,
      ),
      child: Column(
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10,right: 16, top: 10),
                child: Icon(
                  Icons.account_balance,
                  size: 40,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Text(className, style: TextStyle(fontWeight: FontWeight.bold),),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Text(
                    presensi,
                    style: TextStyle(color: (presensi == "Hadir") ? Colors.lightBlueAccent : Colors.red, fontWeight: FontWeight.bold),
                  ),
                ],
              ),

            ],
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10,right: 16, top: 10),
                child: Icon(
                  Icons.date_range,
                  size: 40,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Text(waktu),
              ),
            ],
          ),
        ],
      ),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(5)),
    );
  }

}
