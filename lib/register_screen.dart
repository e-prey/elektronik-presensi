import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  var _emailController = TextEditingController();
  var _passwordController = TextEditingController();
  var _nimController = TextEditingController();
  var _nameController = TextEditingController();

  bool _visible = false;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.lightBlueAccent,
        title: Text("Registrasi Pengguna"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(
            child: Image.asset(
              "assets/burem.png",
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              fit: BoxFit.fill,
            ),
          ),
          Form(
            key: _formKey,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 30, left: 32, right: 32),
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 1),
                  decoration: BoxDecoration(
                    color: Color(0xffC4C4C4),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: TextFormField(
                    controller: _nimController,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: 'No.ID/NIM',
                      labelStyle: TextStyle(color: Colors.black),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                      ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'No.ID/NIM tidak boleh kosong';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.only(top: 10, bottom: 10, left: 32, right: 32),
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 1),
                  decoration: BoxDecoration(
                    color: Color(0xffC4C4C4),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: TextFormField(
                    controller: _passwordController,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: 'Masukkan Kata Sandi',
                      labelStyle: TextStyle(color: Colors.black),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                      ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Kata sandi tidak boleh kosong';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 32, right: 32),
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 1),
                  decoration: BoxDecoration(
                    color: Color(0xffC4C4C4),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: TextFormField(
                    controller: _nameController,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.name,
                    decoration: InputDecoration(
                      labelText: 'Nama Lengkap',
                      labelStyle: TextStyle(color: Colors.black),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                      ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama Lengkap tidak boleh kosong';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10, left: 32, right: 32),
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 1),
                  decoration: BoxDecoration(
                    color: Color(0xffC4C4C4),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: TextFormField(
                    controller: _emailController,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle: TextStyle(color: Colors.black),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                      ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Email Lengkap tidak boleh kosong';
                      } else if (!value.contains("@") || !value.contains(".")) {
                        return 'Format email tidak sesuai';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Visibility(
                    visible: _visible,
                    child: SpinKitRipple(
                      color: Colors.blue,
                    ),
                  ),
                ),
                Container(
                  width: 200,
                  height: 45,
                  margin: EdgeInsets.only(top: 10),
                  child: RaisedButton(
                      color: Colors.lightBlueAccent,
                      child: Text(
                        "Registrasi",
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          setState(() {
                            _visible = true;
                          });

                          await _checkIsUserAlreadyRegisteredOrNot();


                          setState(() {
                            _visible = false;
                          });

                        }
                      }),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _checkIsUserAlreadyRegisteredOrNot() {
    FirebaseFirestore.instance
        .collection("users")
        .where('nim', isEqualTo: _nimController.text.toString())
        .get()
        .then((value) {

          if(value.size == 0) {
            _registrateUser();
          }
          else {
            _toast("NIM ini sudah pernah didaftarkan");
          }

    });

  }

  _registrateUser() async {
    String _time = DateTime.now().millisecondsSinceEpoch.toString();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('math${_nimController.text.toString()}', false);
    prefs.setBool('chemical${_nimController.text.toString()}', false);
    prefs.setBool('physics${_nimController.text.toString()}', false);
    prefs.setBool('biology${_nimController.text.toString()}', false);


    Map<String, Object> data = {
      'name' : _nameController.text.toString(),
      'email' : _emailController.text.toString(),
      'nim' : _nimController.text.toString(),
      'password' : _passwordController.text.toString(),
    };

    FirebaseFirestore.instance
        .collection("users")
        .doc(_time)
        .set(data)
        .then((_) {
          _nameController.clear();
          _emailController.clear();
          _nimController.clear();
          _passwordController.clear();
          _toast("Berhasil mendaftar");

    });
  }

  void _toast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.lightBlueAccent,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
