import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:eprey/register_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home_screen.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.lightBlueAccent,
            title: Text("E-PREY"),
          ),
          resizeToAvoidBottomInset: false,
          body: PopulateBody()),
    );
  }
}

class PopulateBody extends StatefulWidget {
  const PopulateBody({Key? key}) : super(key: key);

  @override
  _PopulateBodyState createState() => _PopulateBodyState();
}

class _PopulateBodyState extends State<PopulateBody> {
  var _emailController = TextEditingController();
  var _passwordController = TextEditingController();
  String role = '';

  bool _visible = false;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          child: Image.asset(
            "assets/burem.png",
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            fit: BoxFit.fill,
          ),
        ),
        Form(
          key: _formKey,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 90, left: 32, right: 32),
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 1),
                decoration: BoxDecoration(
                  color: Color(0xffC4C4C4),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: TextFormField(
                  controller: _emailController,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'No.ID/NIM',
                    labelStyle: TextStyle(color: Colors.black),
                    enabledBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'No.ID/NIM tidak boleh kosong';
                    } else {
                      return null;
                    }
                  },
                ),
              ),
              Container(
                margin:
                    EdgeInsets.only(top: 10, bottom: 20, left: 32, right: 32),
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 1),
                decoration: BoxDecoration(
                  color: Color(0xffC4C4C4),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: TextFormField(
                  controller: _passwordController,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: 'Masukkan Kata Sandi',
                    labelStyle: TextStyle(color: Colors.black),
                    enabledBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xffC4C4C4), width: 1.0),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Kata sandi tidak boleh kosong';
                    } else {
                      return null;
                    }
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Visibility(
                  visible: _visible,
                  child: SpinKitRipple(
                    color: Colors.blue,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 100,
                    height: 45,
                    margin: EdgeInsets.only(left: 35, top: 16),
                    child: RaisedButton(
                        color: Colors.lightBlueAccent,
                        child: Text(
                          "Registrasi",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        onPressed: () {
                          Route route = MaterialPageRoute(
                              builder: (context) => RegisterScreen());
                          Navigator.push(context, route);
                        }),
                  ),
                  Container(
                    width: 100,
                    height: 45,
                    margin: EdgeInsets.only(right: 35, top: 16),
                    child: RaisedButton(
                        color: Colors.lightBlueAccent,
                        child: Text(
                          "Masuk",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            setState(() {
                              _visible = true;
                            });

                            _loginUser();

                            setState(() {
                              _visible = false;
                            });
                          }
                        }),
                  )
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  _loginUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    FirebaseFirestore.instance
        .collection("users")
        .where('nim', isEqualTo: _emailController.text.toString())
        .where('password', isEqualTo: _passwordController.text.toString())
        .get()
        .then((value) {
      print(value.size);
      if (value.size == 1) {
        prefs.setBool('isLogin', true);
        prefs.setString('nim', _emailController.text.toString());
        Route route = MaterialPageRoute(builder: (context) => HomePage());
        Navigator.pushReplacement(context, route);
      } else {
        _toast("Cek kembali data anda, pastikan terisi dengan benar");
      }
    });
  }

  void _toast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.lightBlueAccent,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
